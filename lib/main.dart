import 'package:flutter/material.dart';
import 'package:flutter_shop/views/index.dart';
import 'config/index.dart';
import 'provide/current_index_provide.dart';
import 'package:provide/provide.dart';
import 'views/index.dart';

void main(){
  var currentIndexProvide = CurrentIndexProvide();
  var providers = Providers();

  providers
    ..provide(Provider<CurrentIndexProvide>.value(currentIndexProvide));

  runApp(ProviderNode(
    providers: providers,
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(child:  MaterialApp(
      title: KString.mainTitle, //flutter 商城
      debugShowCheckedModeBanner: false,
      // 主题配置
      theme: ThemeData(
        primaryColor: KColor.primaryColor,
      ),
      home: IndexView(),
    ));
  }
}