class Khttp{
  static const String Base_Url = 'http//:3000/release.com/';
}

class ResultCode{
  /*
   * dio网络请求失败的回调错误码
   */
  //正常返回是1
  static const num SUCCESS = 1;

  //异常返回是0
  static const num ERROR = 1;

  /// When opening  url timeout, it occurs.
  static const num CONNECT_TIMEOUT = -1;

  ///It occurs when receiving timeout.
  static const num RECEIVE_TIMEOUT = -2;

  /// When the server response, but with a incorrect status, such as 404, 503...
  static const num RESPONSE = -3;
  /// When the request is cancelled, dio will throw a error with this type.
  static const num CANCEL = -4;

  /// read the DioError.error if it is not null.
  static const num DEFAULT = -5;
}