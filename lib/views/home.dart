import 'package:provide/provide.dart';
import 'package:flutter/material.dart';
import '../config/index.dart';

class HomeView extends StatefulWidget{
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView>{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(child: Text(KString.homeTitle));
  }
}